## gtel3gxx-user 4.4.4 KTU84P T561XXU0AQI2 release-keys
- Manufacturer: samsung
- Platform: sc8830
- Codename: gtel3g
- Brand: samsung
- Flavor: user
- Release Version: 4.4.4
- Id: KTU84P
- Incremental: T561XXU0AQI2
- Tags: release-keys
- CPU Abilist: 
- A/B Device: false
- Locale: undefined
- Screen Density: 160
- Fingerprint: samsung/gtel3gxx/gtel3g:4.4.4/KTU84P/T561XXU0AQI2:user/release-keys
- OTA version: 
- Branch: gtel3gxx-user-4.4.4-KTU84P-T561XXU0AQI2-release-keys
- Repo: samsung_gtel3g_dump_10711


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
